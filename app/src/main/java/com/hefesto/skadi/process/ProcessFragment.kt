package com.example.skadi.process

import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.hefesto.skadi.temperature.Thermometer
import com.hefesto.skadi.ProcessAdapter
import com.hefesto.skadi.NativeCall
import com.hefesto.skadi.ProcessInfo
import com.hefesto.skadi.R

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ProcessesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProcessesFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    lateinit var adp_pi: ProcessAdapter
    lateinit var lv_proc: ListView
    lateinit var proc_list : List<ProcessInfo>

    lateinit var tv_battery_temp : TextView
    lateinit var tv_cpu_temp : TextView

    var sort_selector : Int = 0

    private val handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_process, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        handler.post(refresh_process)
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        requireActivity().menuInflater.inflate(R.menu.process_context_menu, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val info = item.menuInfo as AdapterView.AdapterContextMenuInfo
        var proc = lv_proc.getItemAtPosition(info.position) as ProcessInfo
        when(item.itemId) {
            R.id.kill -> {
                if (proc != null) {
                    if (ContextCompat.checkSelfPermission(requireContext(), android.Manifest.permission.KILL_BACKGROUND_PROCESSES) == PackageManager.PERMISSION_GRANTED) {
                        android.os.Process.killProcess(proc.pid)
                        Toast.makeText(requireContext(), "Selected process killed.", Toast.LENGTH_LONG).show()
                    }
                    else {
                        Toast.makeText(requireContext(), "No permissions to kill it.", Toast.LENGTH_LONG).show()
                    }
                }
                else
                    Toast.makeText(requireContext(), "No process founded.", Toast.LENGTH_LONG).show()
                return true
            }
            else -> {}
        }
        return super.onContextItemSelected(item)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ProcessesFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ProcessesFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private val refresh_process = object : Runnable {
        override fun run() {
            loadData()
            draw_task()
            handler.postDelayed(this, 5000)
        }
    }

    // Vincula as variaveis aos IDs dos elementos visuais
    private fun initView()
    {
//        tv_cpu_temp = requireView().findViewById(R.id.procmon_tv_cpu_temp)
//        tv_battery_temp = requireView().findViewById(R.id.procmon_tv_battery_temp)
        lv_proc = requireView().findViewById(R.id.list)
        registerForContextMenu(lv_proc)
    }

    // Atualiza a lista de processos
    private fun loadData()
    {
        NativeCall.refreshProcessList()
        proc_list = NativeCall.getProcessInfo()
        proc_list = proc_list.filter { it.owner.subSequence(0, 2) == "u0" }     // filtra apenas os processos de usuarios
        proc_list = when(sort_selector) {
            0 -> proc_list.sortedByDescending { it.cpu_usage }
            1 -> proc_list.sortedByDescending { it.name }
            2 -> proc_list.sortedByDescending { it.pid }
            3 -> proc_list.sortedBy { it.owner }
            else -> proc_list.sortedByDescending { it.cpu_usage }
        }
    }

    // Atualiza a ListViewer
    private fun draw_task(){
        adp_pi = ProcessAdapter(this.requireContext(), R.layout.item_process, proc_list)
        lv_proc.setAdapter(adp_pi)
        lv_proc.invalidateViews()
    }

}