package com.example.skadi.temperature

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.recyclerview.widget.RecyclerView
import com.hefesto.skadi.R
import com.hefesto.skadi.temperature.TemperatureAdapter
import com.hefesto.skadi.temperature.TemperatureInfo
import com.hefesto.skadi.temperature.Thermometer

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TemperatureFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TemperatureFragment : Fragment() {

    lateinit var adp_temp : TemperatureAdapter
    lateinit var lv_temp : ListView
    var battery_temp : Int = 0
    var cpu_temp : Int = 0

    var temp_data: ArrayList<TemperatureInfo> = ArrayList<TemperatureInfo>()

    private val handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_temperature, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        handler.post(refresh_temp)
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TemperatureFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TemperatureFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private val refresh_temp = object : Runnable {
        override fun run() {
            loadData()
            draw_task()
            handler.postDelayed(this, 5000)
        }
    }

    private fun initView()
    {
        lv_temp = requireView().findViewById(R.id.temp_list)
    }

    private fun loadData()
    {
        temp_data.clear()
        var battery_temp_info : TemperatureInfo = TemperatureInfo()
        var cpu_temp_info: TemperatureInfo = TemperatureInfo()
        cpu_temp = Thermometer.getCPUTemperature()
        if (cpu_temp != -31)
        {
            cpu_temp_info.temperature = cpu_temp
            cpu_temp_info.type = "CPU"
            temp_data.add(cpu_temp_info)
        }
        battery_temp = Thermometer.getBatteryTemperature(this.requireContext())
        if (battery_temp != -31)
        {
            battery_temp_info.temperature = battery_temp
            battery_temp_info.type = "Battery"
            temp_data.add(battery_temp_info)
        }
    }

    private fun draw_task()
    {
        adp_temp = TemperatureAdapter(this.requireContext(), R.layout.item_temperature, temp_data)
        lv_temp.setAdapter(adp_temp)
        lv_temp.invalidateViews()
    }

}