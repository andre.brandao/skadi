package com.hefesto.skadi.temperature

import android.app.Activity
import android.content.Context
import android.content.pm.ApplicationInfo
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.hefesto.skadi.R

class TemperatureAdapter(

    private val mcontext: Context,
    private val layout_resource_id: Int,
    private val data : List<TemperatureInfo>
) : ArrayAdapter<TemperatureInfo>(
    mcontext, layout_resource_id, data
) {
    override fun getView(position: Int, convert_view: View?, parent: ViewGroup): View {
        var row = convert_view
        var holder: ViewHolder?
        var ai: ApplicationInfo
        if (row == null) {
            val inflater = (context as Activity).layoutInflater
            row = inflater.inflate(layout_resource_id, parent, false)
            holder = ViewHolder()
            holder.textView1 = row.findViewById<View>(R.id.item_temperature_tv_type) as TextView
            holder.textView2 = row.findViewById<View>(R.id.item_temperature_tv_temp) as TextView
            holder.textView3 = row.findViewById<View>(R.id.item_temperature_tv_status) as TextView
            holder.imageView1 = row.findViewById<View>(R.id.item_temperature_iv_icon) as ImageView
            row.tag = holder
        } else {
            holder = row.tag as ViewHolder
        }

        val temp = data[position]

        holder.textView1!!.setText(temp.type)
        holder.textView2!!.setText(temp.temperature.toString().subSequence(0, 2).padEnd(3,'º').padEnd(4,'C'))
        if(temp.type == "CPU") {
            val status = Thermometer.getCPUThermalStatus(temp.temperature)
            holder.textView3!!.setText(Thermometer.getCPUThermalStatus(temp.temperature))
            holder.textView3!!.setTextColor(Thermometer.getThermalStatusColor(status))
            holder.imageView1!!.setImageResource(R.drawable.icon_cpu)
        } else if (temp.type == "Battery") {
            val status = Thermometer.getBatteryThermalStatus(temp.temperature)
            holder.textView3!!.setText(status)
            holder.textView3!!.setTextColor(Thermometer.getThermalStatusColor(status))
            holder.imageView1!!.setImageResource(R.drawable.icon_battery)
        }
        return row!!
    }

    internal class ViewHolder {
        var textView1: TextView? = null
        var textView2: TextView? = null
        var textView3: TextView? = null
        var imageView1: ImageView? = null
    }
}
